// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  hotelImage: '/assets/recursos/assets/images/hotels/',
  starsImage: '/assets/recursos/assets/icons/filters/star.svg',
  amenityImage: '/assets/recursos/assets/icons/amenities/',
  currency: 'ARS',
  baseUrl: 'http://localhost:3000/'
};
