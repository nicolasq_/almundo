export const environment = {
  production: true,
  hotelImage: '/assets/recursos/assets/images/hotels/',
  starsImage: '/assets/recursos/assets/icons/filters/star.svg',
  amenityImage: '/assets/recursos/assets/icons/amenities/',
  currency: 'ARS',
  baseUrl: 'http://localhost:3000/'
};
