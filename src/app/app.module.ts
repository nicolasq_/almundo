import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HotelsComponent } from './components/hotels/hotels.component';
import { HttpService } from './core/services/http.service';
import { HotelsService } from './core/services/hotels.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HotelStructureComponent } from './components/hotel-structure/hotel-structure.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { FiltersComponent } from './components/filters/filters.component';


@NgModule({
  declarations: [
    AppComponent,
    HotelsComponent,
    HotelStructureComponent,
    FiltersComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatInputModule,
    MatFormFieldModule,
    MatRadioModule
  ],
  providers: [HttpService, HotelsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
