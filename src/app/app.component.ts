import { Component, OnInit } from '@angular/core';
import { HttpService } from './core/services/http.service';
import { HotelsService } from './core/services/hotels.service';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit  {

  constructor(private httpService: HttpService,
              private hotelsService: HotelsService){}


  ngOnInit(){
    this.populateSystem();
  }

  private populateSystem(): void {
    this.httpService.get(environment.baseUrl + 'hotel/').subscribe(hotels => {
      if(hotels.length === 0) {
        this.httpService.get('/assets/recursos/data/data.json').subscribe(hotels => {
          this.httpService.post(environment.baseUrl + 'hotel/first_save', hotels).subscribe(hotels => {
            this.hotelsService.hotels.next(hotels.ops);
          })
        })
      }
    })
  }
}
