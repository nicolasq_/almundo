import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../../core/services/hotels.service';
import { HttpService } from '../../core/services/http.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  constructor(private httpService: HttpService,
    private hotelsService: HotelsService) { }

  ngOnInit() {
  }

  public onSearchChange(searchValue : string ) {  
    if(searchValue !== ''){
      this.httpService.get(environment.baseUrl + 'hotel/name/'+ searchValue).subscribe(hotels => {
      this.hotelsService.hotels.next(hotels);
    })
    }else {
      this.hotelsService.getAll().subscribe();
    }
    
  }

  public getStars(value: number): Array<number>{
    return new Array(value);
  }

  public getHotelsWithStars(stars: Number): void {
    this.httpService.get(environment.baseUrl + 'hotel/stars/' + stars).subscribe(hotels => {
      this.hotelsService.hotels.next(hotels);
    })
  }

}
