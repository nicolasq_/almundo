import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelStructureComponent } from './hotel-structure.component';

describe('HotelStructureComponent', () => {
  let component: HotelStructureComponent;
  let fixture: ComponentFixture<HotelStructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelStructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelStructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
