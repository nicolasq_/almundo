import { Component, OnInit, Input } from '@angular/core';
import { Hotel } from '../../core/models/hotel';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-hotel-structure',
  templateUrl: './hotel-structure.component.html',
  styleUrls: ['./hotel-structure.component.scss']
})
export class HotelStructureComponent implements OnInit {

  @Input() hotel: Hotel;

  public range;
  
  constructor() { }

  ngOnInit() {
    this.range = new Array(this.hotel.stars);
  }

  public getHotelImg(url): string {
    return environment.hotelImage + url;
  }

  public getStarsImg(): string {
    return environment.starsImage;
  }

  public getAmenityImg(url): string {
    return `${environment.amenityImage}${url}.svg`;
  }

  public getCurrency(): string {
    return environment.currency;
  }
}