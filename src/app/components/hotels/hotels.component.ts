import { Component, OnInit } from '@angular/core';
import { HotelsService } from '../../core/services/hotels.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {

  constructor(public hotelsService: HotelsService) { }

  ngOnInit() {
    this.getAllHotels();
  }

  private getAllHotels(): void {
    this.hotelsService.getAll().subscribe(hotels => { },
      (err: HttpErrorResponse) => {
        console.log(err);
      });
  }
}
