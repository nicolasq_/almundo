import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable()

export class HttpService {

    public headerLocal;
    public options;

    constructor(
        private http: HttpClient
    ) { }


    get(url): Observable<any> {

        return this.http.get(this.getUrl(url));
    }

    post(url, data?): Observable<any> {

        return this.http.post(this.getUrl(url), data);
    }

    put(url, data?): Observable<any> {
        return this.http.put(this.getUrl(url), data);
    }


    delete(url): Observable<any> {

        return this.http.delete(this.getUrl(url));
    }

    private getUrl(url): string {
        return url;
    }

}
