import { Injectable } from '@angular/core';
import { Hotel } from '../models/hotel';
import { Observable } from 'rxjs/Observable';
import { HttpService } from './http.service';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/do';
import { environment } from '../../../environments/environment.prod';

@Injectable()
export class HotelsService {

  public hotels: Subject<Hotel> = new Subject<Hotel>();

  constructor(private httpService: HttpService) { }


  public getAll(): Observable<Hotel> {
    return this.httpService
      .get(environment.baseUrl + 'hotel/')
      .do((data: any) => {
        this.hotels.next(data);
      });
  }
}
