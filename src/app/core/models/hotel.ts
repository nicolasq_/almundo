export interface HotelInterface {

    id: string;
    name: string;
    stars: number;
    price: number;
    image: string;
    amenities: Array<string>;
}


export class Hotel implements HotelInterface {

    public id: string;
    public name: string;
    public stars: number;
    public price: number;
    public image: string;
    public amenities: Array<string>;

    constructor(init) {
        Object.assign(this, init);
    }
}
