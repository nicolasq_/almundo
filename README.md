**AlMundo Test**

Prueba Realizada por Jose Nicolas Quijada.

La prueba se realizo con la finalidad de optar al cargo ofrecido por la compañia almundo.
La misma se realizo con JS, Frontend: Angular 5 y Backend: NodeJS

El sistema requiere MongoDB: 
 - sudo apt install mongodb-server

Para inicializar el proyecto, basta con correr en consola el comando npm start. 
De manera concurrente se inicializara el backend y el frontend.

La data proporcionada por almundo, en ingresada en una base de datos no relacional (mongoDB), donde posteriormente son obtenidos los datos.

Herramientas utilizadas:

Frontend:

-- Angular
-- FlexboxGrid
-- Angular Material
-- RxJS
-- Angular CLI

Backend:

-- MongoDB
-- bluebird
-- body-parser
-- cors
-- express
-- mongoose
-- concurrently

    Endpoints:

    - Todos los hoteles: (GET)
        http://localhost:3000/hotel/
    
    - Obtener por ID: (GET)
        http://localhost:3000/hotel/:id

    - Obtener por nombre: (GET)
        http://localhost:3000/hotel/name/:name
    
    - Obtener por estrellas: (GET)
        http://localhost:3000/hotel/stars/:stars
    
    - Agregar nuevo hotel: (POST)
        http://localhost:3000/hotel/

    - Agregar todos los hoteles del archivo: (POST)
        http://localhost:3000/hotel/first_save

    - Actualizar hotel: (PUT)
        http://localhost:3000/hotel/

    - Eliminar hotel: (DELETE)
        http://localhost:3000/hotel/