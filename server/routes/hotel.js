var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Hotel = require('../models/Hotel.js');


/* GET ALL HOTELS */
router.get('/', function (req, res, next) {
    Hotel.find(function (err, hotels) {
        if (!err) {
            res.json(hotels);
        } else {
            return next(err);
        }
    });
});

/* GET BY STARS */
router.get('/stars/:stars', function (req, res, next) {
    Hotel.find({"stars": req.params.stars}, function (err, hotels) {
        if (!err) {
            res.json(hotels);
        } else {
            return next(err);
        }
    });
});

/* GET SINGLE HOTEL BY ID */
router.get('/:id', function (req, res, next) {
    Hotel.find({ "id": req.params.id}, function (err, hotel) {
        if (!err) {
            res.json(hotel);
        } else {
            return next(err);
        }
    });
});

/* GET BY NAME */

router.get('/name/:name', function (req, res, next) {
    Hotel.find({ "name": new RegExp(req.params.name, 'i')}, function (err, hotels) {
        if (!err) {
            res.json(hotels);
        } else {
            return next(err);
        }
    });
});

/* SAVE HOTEL */
router.post('/', function (req, res, next) {
    Hotel.create(req.body, function (err, post) {
        if (!err) {
            res.json(post);
        } else {
            return next(err);
        }
    });
});

/* FIRST SAVE */
router.post('/first_save', function (req, res, next) {

    Hotel.collection.insertMany(req.body,
        function (err, post) {
            if (!err) {
                res.json(post);
            } else {
                return next(err);
            }
        });
});

/* UPDATE HOTEL */
router.put('/:id', function (req, res, next) {
    Hotel.find({"id": req.params.id}, function (err, hotel) {
        if (!err) {
            Hotel.findByIdAndUpdate(hotel[0]._id, req.body, function(err, hotel){
                res.json(hotel);
            })
        } else {
            return next(err);
        }
    });
});

/* DELETE HOTEL */

router.delete('/:id', function (req, res, next) {
    Hotel.find({"id": req.params.id}, function(err, hotel){
        Hotel.findByIdAndRemove(hotel[0]._id, req.body, function (err, post) {
            if (!err) {
                res.json(post);
            } else {
                return next(err);
            }
        });
    })
});


module.exports = router;